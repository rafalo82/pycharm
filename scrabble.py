scores = {"a": 1, "c": 3, "b": 3, "e": 1, "d": 2, "g": 2,
"f": 4, "i": 1, "h": 4, "k": 5, "j": 8, "m": 3,
"l": 1, "o": 1, "n": 1, "q": 10, "p": 3, "s": 1,
"r": 1, "u": 1, "t": 1, "w": 4, "v": 4, "y": 4,
"x": 8, "z": 10}

def scrabble_score(word):
    total_score = 0
    for letter in word.lower():
        total_score += scores[letter] # lub lepsza wersja -> total_score = scores.get(letter)
    return total_score

if __name__ == '__main__':
    while True:
        my_word = input('Give a word: ').strip()
        score = scrabble_score(my_word)
        print(f'Word {my_word} is worth {score} in Scrabble')
        shall_continue = input('Do you want to continue ([y]/n])?: ')
        if shall_continue.lower() != 'y':
            break

scrabble_score(my_word)